﻿using System;
using TESMERTV.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(CloseHelperDroid))]
namespace TESMERTV.Droid
{
	public class CloseHelperDroid : CloseHelper
	{
		public CloseHelperDroid()
		{
		}

		public void CloseApp()
		{
			Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
		}
	}
}
