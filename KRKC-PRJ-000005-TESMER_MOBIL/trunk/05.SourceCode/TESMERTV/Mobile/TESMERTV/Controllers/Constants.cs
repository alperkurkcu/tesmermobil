﻿using System;
using Plugin.Connectivity;

namespace TESMERTV
{
	static class Constants
	{
		public static string LogoPath = "TESMERTV.Media.Images.logo.jpg";
		public static string SplashPath = "TESMERTV.Media.Images.tesmer.gif";
		public static bool ConnectionStatus = false;

		public static void CheckInternet()
		{
			if (CrossConnectivity.Current.IsConnected)
			{ ConnectionStatus = true; }
			else { ConnectionStatus = false; 
			}
		}

		public static string PagePath = "http://tesmertv.tesmer.org.tr/";
	}
}
