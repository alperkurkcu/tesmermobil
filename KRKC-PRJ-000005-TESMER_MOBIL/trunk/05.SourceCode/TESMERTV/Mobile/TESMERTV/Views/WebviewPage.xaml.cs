﻿using Xamarin.Forms;

namespace TESMERTV
{
	public partial class WebviewPage : ContentPage
	{

		private const int PADDING_WIDTH = 0;
		private int paddingHeight;

		public WebviewPage()
		{
			InitializeComponent();

			InitializeImages();

			if (Constants.ConnectionStatus)
			{
				Browser.VerticalOptions = LayoutOptions.FillAndExpand;
				Browser.HorizontalOptions = LayoutOptions.FillAndExpand;
				Browser.Source = Constants.PagePath;
			}
			else {
				Alert();
			}

			CheckDevice();

			MainStackLayout.Padding = new Thickness(PADDING_WIDTH, paddingHeight);
			MainStackLayout.Spacing = 0.1;

		}

		protected void InitializeImages()
		{
			//LogoBanner.Source = ImageSource.FromResource(Constants.LogoPath);

			//var tapImage = new TapGestureRecognizer();
			//tapImage.Tapped += (sender, e) =>
			//{
			//	//A.K. - internet kontrol ediliyor, yok ise Alert verilip uygulama kapatılıyor
			//	Constants.CheckInternet();
			//	if (!Constants.ConnectionStatus)
			//		Alert();

			//	Browser.Source = Constants.PagePath;
			//};
			//LogoBanner.GestureRecognizers.Add(tapImage);
		}

		private void CheckDevice()
		{
			if (Device.OS == TargetPlatform.Android)
			{
				SetPaddingHeight(0);
			}
			else if (Device.OS == TargetPlatform.iOS)
			{
				SetPaddingHeight(20);
			}
		}

		public void SetPaddingHeight(int pHeight)
		{
			paddingHeight = pHeight;
		}

		//protected override bool OnBackButtonPressed()
		//{
		//	if (Browser.CanGoBack)
		//	{
		//		Browser.GoBack();
		//		return true;
		//	}

		//	return false;
		//}

		public async void Alert()
		{
			await DisplayAlert("İnternet İhtiyacı", "İnternet bağlantısı bulunamadı, lütfen internet ayarlarınızı kontrol ediniz.", "OK");

			if (Device.OS == TargetPlatform.Android)
				DependencyService.Get<CloseHelper>().CloseApp();
		}
	}
}