﻿using System;
using EUSE.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(CloseHelperDroid))]
namespace EUSE.Droid
{
	public class CloseHelperDroid : CloseHelper
	{
		public void CloseApp()
		{
			Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
		}
	}
}

