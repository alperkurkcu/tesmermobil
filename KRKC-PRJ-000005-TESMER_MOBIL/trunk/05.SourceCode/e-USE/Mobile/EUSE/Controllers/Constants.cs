﻿using System;
using Plugin.Connectivity;

namespace EUSE
{
	static class Constants
	{
		public static string LogoPath = "EUSE.Media.Images.banner.jpg";
		public static string SplashPath = "EUSE.Media.Images.e-use_logo_mini.jpg";
		public static bool ConnectionStatus = false;

		public static void CheckInternet()
		{
			if (CrossConnectivity.Current.IsConnected)
			{ ConnectionStatus = true; }
			else {
				ConnectionStatus = false;
			}
		}

		public static string PagePath = "http://euse2.tesmer.org.tr/mobile";
	}
}

