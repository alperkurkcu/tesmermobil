﻿using System;
using Android.Content;
using Java.IO;
using TESMER.Droid;
using Xamarin.Forms;
using Uri = Android.Net.Uri;

[assembly: Dependency(typeof(DownloadHelperDroid))]
namespace TESMER.Droid
{
	public class DownloadHelperDroid : DownloadHelper
	{

		public void Downloader(string uri)
		{

			char[] delimiterChar = { '/' };
			string[] words = uri.Split(delimiterChar);
			string fileName = words[words.Length - 1];

			if (!FileExist(fileName))
			{
				Android.Net.Uri contentUri = Android.Net.Uri.Parse(uri);

				Android.App.DownloadManager.Request r = new Android.App.DownloadManager.Request(contentUri);

				r.SetDestinationInExternalPublicDir(Android.OS.Environment.DirectoryDownloads, fileName);

				r.AllowScanningByMediaScanner();

				r.SetNotificationVisibility(Android.App.DownloadVisibility.VisibleNotifyCompleted);

				Android.App.DownloadManager dm = (Android.App.DownloadManager)Xamarin.Forms.Forms.Context.GetSystemService(Android.Content.Context.DownloadService);

				dm.Enqueue(r);
			}


			ShowPdfFile(fileName);

		}

		public void ShowPdfFile(string fileName)
		{

			Java.IO.File path = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads);
			Java.IO.File file = new Java.IO.File(path, fileName);

			if (!FileExist(fileName))
				return;

			var intent = DisplayPdf(file);
			Forms.Context.StartActivity(intent);
		}

		public Intent DisplayPdf(File file)
		{
			var intent = new Intent(Intent.ActionView);
			var filepath = Uri.FromFile(file);
			intent.SetDataAndType(filepath, "application/pdf");
			intent.SetFlags(ActivityFlags.ClearTop);
			return intent;
		}

		public bool FileExist(string fileName)
		{

			Java.IO.File path = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads);
			Java.IO.File file = new Java.IO.File(path, fileName);

			bool fileControl = true;

			if (!file.Exists())
				return false;

			return fileControl;
		}


	}
}
