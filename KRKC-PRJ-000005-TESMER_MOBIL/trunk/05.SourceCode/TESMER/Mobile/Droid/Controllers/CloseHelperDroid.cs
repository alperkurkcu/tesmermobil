﻿using System;
using TESMER.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(CloseHelperDroid))]
namespace TESMER.Droid
{
	public class CloseHelperDroid : CloseHelper
	{

		public void CloseApp()
		{
			Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
		}
	}
}
