﻿using System;
using Plugin.Connectivity;

namespace TESMER
{
	static class Constants
	{
		public static string LogoPath = "TESMER.Media.Images.logo.jpg";
		public static string SplashPath = "TESMER.Media.Images.tesmer.gif";
		public static bool ConnectionStatus = false;

		public static void CheckInternet()
		{
			if (CrossConnectivity.Current.IsConnected)
			{ ConnectionStatus = true; }
			else { ConnectionStatus = false; 
			}
		}

		public static string PagePath = "http://www.tesmer.org.tr/mobile";
	}
}
