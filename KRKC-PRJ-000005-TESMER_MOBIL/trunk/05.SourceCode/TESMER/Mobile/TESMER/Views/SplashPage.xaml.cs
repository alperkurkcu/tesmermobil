﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace TESMER
{
	public partial class SplashPage : ContentPage
	{
		public SplashPage()
		{
			Content = new Image
			{
				Source = ImageSource.FromResource(Constants.SplashPath),
				VerticalOptions = LayoutOptions.Center,
				HorizontalOptions = LayoutOptions.Center
			};

			InitializeComponent();

			ToLoginPage();
		}

		async void ToLoginPage()
		{
			await Task.Delay(1000);
			await Navigation.PushModalAsync(new WebviewPage());
		}
	}
}

