﻿using Plugin.Connectivity;
using Xamarin.Forms;

namespace TESMER
{
	public partial class App : Application
	{
		public App()
		{
			InitializeComponent();

			MainPage = new SplashPage();
		}

		protected override void OnStart()
		{
			// Handle when your app starts
			Constants.CheckInternet();
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
			Constants.CheckInternet();
		}
	}
}
